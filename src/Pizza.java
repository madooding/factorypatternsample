
public abstract class Pizza {
	public abstract void prepare();
	public void bake(){
		System.out.println("Pizza is baking ..");
	}
	
	public void cut(){
		System.out.println("Pizza is now cutting ...");
	}
	
	public void box(){
		System.out.println("Now the pizza has put in the box already.");
	}
	
	
}
