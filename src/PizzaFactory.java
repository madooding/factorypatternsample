
public class PizzaFactory {
	public Pizza createPizza(String type){
		Pizza p = null;
		
		if(type.equals("Tomyum")){
			p = new TomyumPizza();
		}else if(type.equals("Pepperoni")){
			p = new PepperoniPizza();
		}
		return p;
	}
	
}
