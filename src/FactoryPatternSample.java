
public class FactoryPatternSample {
	public static void main(String[] args){
		PizzaStore pStore = new PizzaStore();
		pStore.orderPizza("Tomyum");
		pStore.orderPizza("Pepperoni");
	}

}
