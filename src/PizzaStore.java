
public class PizzaStore {
	private PizzaFactory pFactory = new PizzaFactory();
	
	public Pizza orderPizza(String type){
		Pizza p;
		p = pFactory.createPizza(type);
		p.prepare();
		p.bake();
		p.cut();
		p.box();
		return p;
	}
}
